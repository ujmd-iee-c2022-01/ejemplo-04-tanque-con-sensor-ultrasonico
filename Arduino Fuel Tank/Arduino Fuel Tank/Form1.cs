﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arduino_Fuel_Tank
{
    public partial class Form1 : Form
    {
        // Variables globales dentro del formulario
        double DistanciaMedida = 0;
        double NivelGasolina = 0;
        double VolumenGasolina = 0;

        double AlturaTanque = 0;
        double RadioTanque = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // El TRY intenta ejecutar un código y, si éste falla,
            // permite ejecutar un código con los datos de la razón
            // de falla en una sección llamada CATCH.
            try
            {
                // Apertura de puerto serial del Arduino
                serialPort1.Open();


                nudDistanciaUltrasónico.Controls[0].Visible = false;
                nudNivelGasolina.Controls[0].Visible = false;
                nudVolumenGasolina.Controls[0].Visible = false;

                AlturaTanque = Convert.ToDouble(nudAlturaTanque.Value);
                RadioTanque = Convert.ToDouble(nudDiametroTanque.Value)/2;
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo abrir el puerto. Detalles:\n" + ex.Message);
                this.Dispose();
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (serialPort1.BytesToRead > 0)
            {
                string TextoRecibido = serialPort1.ReadLine();
                if (TextoRecibido.Length > 0)
                {
                    //  TextoRecibido tendría este formato ==>   "distancia:197\r"

                    string[] TextoSeccionado = TextoRecibido.Split(':','\r');
                    // TextoSeccionado para el ejemplo de formato quedaría así:
                    //       TextoSeccionado[0] = "distancia"
                    //       TextoSeccionado[1] = "197"
                    //       TextoSeccionado[2] = ""

                    if (TextoSeccionado.Length == 3)
                    {
                        string NombreVariable = TextoSeccionado[0];
                        string ValorVariable = TextoSeccionado[1];

                        if (NombreVariable == "distancia")
                        {
                            // Obtención de la distancia medida por el sensor
                            DistanciaMedida = Convert.ToDouble(ValorVariable);
                        }
                    }
                }
            }
        }

        private void tmrActualizarDatos_Tick(object sender, EventArgs e)
        {
            // Primero muestro la distancia medida
            nudDistanciaUltrasónico.Value = Convert.ToDecimal(DistanciaMedida);

            // Actualización de variables de altura y radio según controles del formulario
            AlturaTanque = Convert.ToDouble(nudAlturaTanque.Value);
            RadioTanque = Convert.ToDouble(nudDiametroTanque.Value) / 2;

            // Cálculo del nivel y el volumen
            NivelGasolina = AlturaTanque - DistanciaMedida;
            // El radio y el nivel de gasolina se divide entre 100 por estar en CM.
            // Esto para que el resultado esté en metros cúbicos y, para pasarlo a Litros,
            // se multiplica por 100^2 (es decir mil).
            VolumenGasolina = Math.PI * (RadioTanque/100) * (RadioTanque / 100) * (NivelGasolina / 100) * 1000;

            //Muestro los valores calculados del nivel y el volumen
            nudNivelGasolina.Value = Convert.ToDecimal(NivelGasolina);
            nudVolumenGasolina.Value = Convert.ToDecimal(VolumenGasolina);

            /*
                ACTUALIZACIÓN DE REPRESETACIÓN GRÁFICA DEL TANQUE CON LOS PICTUREBOX
                Altura del Tanque   <==>   PanelTanque.Height
                Nivel de gasolina   <==>   PanelGasolina.Height

                                                                         panelTanque.Height (PX)
                panelGasolina.Height (PX) =   Nivel de gasolina (CM) * ----------------------------
                                                                          Altura del Tanque (CM)

            */
            double Relación = panelTanque.Height / AlturaTanque;
            panelGasolina.Height = (int) (NivelGasolina * Relación);

            // Código para representar el nivel en el TRACKBAR
            trackBar1.Maximum = (int)nudAlturaTanque.Value;
            trackBar1.TickFrequency = trackBar1.Maximum / 10;
            if (NivelGasolina >= 0)
            {
                trackBar1.Value = (int)NivelGasolina;
            }
            else
            {
                trackBar1.Value = 0;
            }
        }

        private void nudAlturaTanque_ValueChanged(object sender, EventArgs e)
        {
            trackBar1.Maximum = (int) nudAlturaTanque.Value;
            trackBar1.TickFrequency = trackBar1.Maximum / 10;
        }
    }
}
