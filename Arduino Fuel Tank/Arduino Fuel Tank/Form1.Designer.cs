﻿
namespace Arduino_Fuel_Tank
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudAlturaTanque = new System.Windows.Forms.NumericUpDown();
            this.nudDistanciaUltrasónico = new System.Windows.Forms.NumericUpDown();
            this.nudNivelGasolina = new System.Windows.Forms.NumericUpDown();
            this.nudVolumenGasolina = new System.Windows.Forms.NumericUpDown();
            this.nudDiametroTanque = new System.Windows.Forms.NumericUpDown();
            this.panelNegro = new System.Windows.Forms.Panel();
            this.panelGris = new System.Windows.Forms.Panel();
            this.panelRojo = new System.Windows.Forms.Panel();
            this.panelBlanco = new System.Windows.Forms.Panel();
            this.panelTanque = new System.Windows.Forms.Panel();
            this.panelGasolina = new System.Windows.Forms.Panel();
            this.tmrActualizarDatos = new System.Windows.Forms.Timer(this.components);
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAlturaTanque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDistanciaUltrasónico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNivelGasolina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVolumenGasolina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiametroTanque)).BeginInit();
            this.panelNegro.SuspendLayout();
            this.panelGris.SuspendLayout();
            this.panelRojo.SuspendLayout();
            this.panelBlanco.SuspendLayout();
            this.panelTanque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Arduino_Fuel_Tank.Properties.Resources.Ultrasonico;
            this.pictureBox1.Location = new System.Drawing.Point(324, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.nudAlturaTanque, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.nudDistanciaUltrasónico, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.nudNivelGasolina, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.nudVolumenGasolina, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.nudDiametroTanque, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(324, 63);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(231, 206);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Distancia medida (cm):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 35);
            this.label5.TabIndex = 0;
            this.label5.Text = "Volumen de gasolina (L):";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 35);
            this.label4.TabIndex = 0;
            this.label4.Text = "Diámetro del tanque (cm):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Altura del tanque (cm):";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 35);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nivel de gasolina (cm):";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nudAlturaTanque
            // 
            this.nudAlturaTanque.DecimalPlaces = 2;
            this.nudAlturaTanque.Location = new System.Drawing.Point(141, 108);
            this.nudAlturaTanque.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudAlturaTanque.Name = "nudAlturaTanque";
            this.nudAlturaTanque.Size = new System.Drawing.Size(87, 20);
            this.nudAlturaTanque.TabIndex = 1;
            this.nudAlturaTanque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudAlturaTanque.Value = new decimal(new int[] {
            202,
            0,
            0,
            0});
            this.nudAlturaTanque.ValueChanged += new System.EventHandler(this.nudAlturaTanque_ValueChanged);
            // 
            // nudDistanciaUltrasónico
            // 
            this.nudDistanciaUltrasónico.DecimalPlaces = 2;
            this.nudDistanciaUltrasónico.Location = new System.Drawing.Point(141, 3);
            this.nudDistanciaUltrasónico.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudDistanciaUltrasónico.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudDistanciaUltrasónico.Name = "nudDistanciaUltrasónico";
            this.nudDistanciaUltrasónico.ReadOnly = true;
            this.nudDistanciaUltrasónico.Size = new System.Drawing.Size(87, 20);
            this.nudDistanciaUltrasónico.TabIndex = 2;
            this.nudDistanciaUltrasónico.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudNivelGasolina
            // 
            this.nudNivelGasolina.DecimalPlaces = 2;
            this.nudNivelGasolina.Location = new System.Drawing.Point(141, 38);
            this.nudNivelGasolina.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudNivelGasolina.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudNivelGasolina.Name = "nudNivelGasolina";
            this.nudNivelGasolina.ReadOnly = true;
            this.nudNivelGasolina.Size = new System.Drawing.Size(87, 20);
            this.nudNivelGasolina.TabIndex = 2;
            this.nudNivelGasolina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVolumenGasolina
            // 
            this.nudVolumenGasolina.DecimalPlaces = 2;
            this.nudVolumenGasolina.Location = new System.Drawing.Point(141, 73);
            this.nudVolumenGasolina.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVolumenGasolina.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudVolumenGasolina.Name = "nudVolumenGasolina";
            this.nudVolumenGasolina.ReadOnly = true;
            this.nudVolumenGasolina.Size = new System.Drawing.Size(87, 20);
            this.nudVolumenGasolina.TabIndex = 2;
            this.nudVolumenGasolina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudDiametroTanque
            // 
            this.nudDiametroTanque.DecimalPlaces = 2;
            this.nudDiametroTanque.Location = new System.Drawing.Point(141, 143);
            this.nudDiametroTanque.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudDiametroTanque.Name = "nudDiametroTanque";
            this.nudDiametroTanque.Size = new System.Drawing.Size(87, 20);
            this.nudDiametroTanque.TabIndex = 1;
            this.nudDiametroTanque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudDiametroTanque.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // panelNegro
            // 
            this.panelNegro.BackColor = System.Drawing.Color.Black;
            this.panelNegro.Controls.Add(this.panelGris);
            this.panelNegro.Location = new System.Drawing.Point(21, 7);
            this.panelNegro.Name = "panelNegro";
            this.panelNegro.Padding = new System.Windows.Forms.Padding(4);
            this.panelNegro.Size = new System.Drawing.Size(282, 348);
            this.panelNegro.TabIndex = 3;
            // 
            // panelGris
            // 
            this.panelGris.BackColor = System.Drawing.Color.DimGray;
            this.panelGris.Controls.Add(this.trackBar1);
            this.panelGris.Controls.Add(this.panelRojo);
            this.panelGris.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGris.Location = new System.Drawing.Point(4, 4);
            this.panelGris.Name = "panelGris";
            this.panelGris.Padding = new System.Windows.Forms.Padding(20);
            this.panelGris.Size = new System.Drawing.Size(274, 340);
            this.panelGris.TabIndex = 0;
            // 
            // panelRojo
            // 
            this.panelRojo.BackColor = System.Drawing.Color.Red;
            this.panelRojo.Controls.Add(this.panelBlanco);
            this.panelRojo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelRojo.Location = new System.Drawing.Point(20, 20);
            this.panelRojo.Name = "panelRojo";
            this.panelRojo.Padding = new System.Windows.Forms.Padding(4);
            this.panelRojo.Size = new System.Drawing.Size(210, 300);
            this.panelRojo.TabIndex = 1;
            // 
            // panelBlanco
            // 
            this.panelBlanco.BackColor = System.Drawing.Color.White;
            this.panelBlanco.Controls.Add(this.panelTanque);
            this.panelBlanco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBlanco.Location = new System.Drawing.Point(4, 4);
            this.panelBlanco.Name = "panelBlanco";
            this.panelBlanco.Padding = new System.Windows.Forms.Padding(4);
            this.panelBlanco.Size = new System.Drawing.Size(202, 292);
            this.panelBlanco.TabIndex = 2;
            // 
            // panelTanque
            // 
            this.panelTanque.BackColor = System.Drawing.Color.Black;
            this.panelTanque.Controls.Add(this.panelGasolina);
            this.panelTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTanque.Location = new System.Drawing.Point(4, 4);
            this.panelTanque.Name = "panelTanque";
            this.panelTanque.Size = new System.Drawing.Size(194, 284);
            this.panelTanque.TabIndex = 2;
            // 
            // panelGasolina
            // 
            this.panelGasolina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(165)))), ((int)(((byte)(55)))));
            this.panelGasolina.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGasolina.Location = new System.Drawing.Point(0, 188);
            this.panelGasolina.Name = "panelGasolina";
            this.panelGasolina.Size = new System.Drawing.Size(194, 96);
            this.panelGasolina.TabIndex = 0;
            // 
            // tmrActualizarDatos
            // 
            this.tmrActualizarDatos.Enabled = true;
            this.tmrActualizarDatos.Tick += new System.EventHandler(this.tmrActualizarDatos_Tick);
            // 
            // trackBar1
            // 
            this.trackBar1.Enabled = false;
            this.trackBar1.LargeChange = 20;
            this.trackBar1.Location = new System.Drawing.Point(233, 14);
            this.trackBar1.Maximum = 202;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 310);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 20;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 369);
            this.Controls.Add(this.panelNegro);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Arduino Fuel Tank";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudAlturaTanque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDistanciaUltrasónico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNivelGasolina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVolumenGasolina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiametroTanque)).EndInit();
            this.panelNegro.ResumeLayout(false);
            this.panelGris.ResumeLayout(false);
            this.panelGris.PerformLayout();
            this.panelRojo.ResumeLayout(false);
            this.panelBlanco.ResumeLayout(false);
            this.panelTanque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudAlturaTanque;
        private System.Windows.Forms.NumericUpDown nudDiametroTanque;
        private System.Windows.Forms.NumericUpDown nudDistanciaUltrasónico;
        private System.Windows.Forms.NumericUpDown nudNivelGasolina;
        private System.Windows.Forms.NumericUpDown nudVolumenGasolina;
        private System.Windows.Forms.Panel panelNegro;
        private System.Windows.Forms.Panel panelGris;
        private System.Windows.Forms.Panel panelRojo;
        private System.Windows.Forms.Panel panelBlanco;
        private System.Windows.Forms.Panel panelTanque;
        private System.Windows.Forms.Panel panelGasolina;
        private System.Windows.Forms.Timer tmrActualizarDatos;
        private System.Windows.Forms.TrackBar trackBar1;
    }
}

