/*
  Ultrasonic Tank Level

  Programa de arduino que envía el valor de la distancia medida por un 
  sensor ultrasónico a la computadora por medio de la comunicación
  Serial-USB.
  
  El dato será recibido por una aplicación de .NET Framework,
  por lo tanto se enviará con el siguiente formato para que la
  aplicación lo pueda interpretar.
         "variable:valor"

  Tanto el programa de arduino como el código fuente de Visual Studio
  están disponibles en el repositorio de GitLab del siguiente enlace:
  https://gitlab.com/ujmd-iee-c2022-01/ejemplo-04-tanque-con-sensor-ultrasonico.git
*/

#include <Ultrasonic.h>

Ultrasonic ultrasonic(7, 8);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int distancia = ultrasonic.read(CM);
  
  // Temporal para facilitar las pruebas porque no tengo en el techo el arduino
  /*distancia = 202 - distancia;
  if(distancia < 0){
    distancia = 0;
  }*/
  // Temporal
  
  Serial.print("distancia:");
  Serial.println(distancia);
  delay(1000);
}
